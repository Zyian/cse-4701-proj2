package main

import (
	"database/sql"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/olekukonko/tablewriter"

	_ "github.com/mattn/go-oci8"

	"github.com/Sirupsen/logrus"
)

var (
	top5Query = `
		SELECT * FROM (
			SELECT "CSE4701"."G0"."SETID",
					count(*) OVERLAP
			FROM "CSE4701"."G0", "CSE4701"."QUERY"
			WHERE "CSE4701"."G0"."GENEID" = "CSE4701"."QUERY"."GENEID"
			AND "CSE4701"."QUERY"."SETID" = %d
			GROUP BY "CSE4701"."G0"."SETID"
			ORDER BY OVERLAP DESC
		)
		WHERE rownum < 6
	`
	numQuery = `
		SELECT DISTINCT "CSE4701"."QUERY"."SETID"
		FROM "CSE4701"."QUERY"
	`
)

func main() {
	db, err := sql.Open("oci8", os.Getenv("CONN_STRING"))
	if err != nil {
		logrus.Errorf("%v", err)
		return
	}
	defer db.Close()

	if err = db.Ping(); err != nil {
		fmt.Printf("Error connecting to the database: %s\n", err)
		return
	}

	setnum, _ := db.Query(numQuery)
	defer setnum.Close()
	var num int
	for setnum.Next() {
		num++
	}

	FindResults(num, db, strings.Replace(top5Query, "G0", "G1", -1))
	FindResults(num, db, strings.Replace(top5Query, "G0", "G2", -1))

}

func FindResults(num int, db *sql.DB, top5QueryS string) {
	for i := 1; i <= num; i++ {
		table := tablewriter.NewWriter(os.Stdout)
		tStart := time.Now()
		values, err := db.Query(fmt.Sprintf(top5QueryS, i))
		if err != nil {
			fmt.Println(err)
			return
		}
		defer values.Close()

		table.SetHeader([]string{
			fmt.Sprintf("Set: %d | Time: %s", i, time.Since(tStart)),
			"Set ID",
			"Overlaps",
		})

		for values.Next() {
			var (
				setid    sql.NullString
				overlaps sql.NullString
			)
			if err := values.Scan(&setid, &overlaps); err != nil {
				fmt.Println(err)
			} else {
				table.Append([]string{"", setid.String, overlaps.String})
			}
		}
		table.Render()
	}
}
